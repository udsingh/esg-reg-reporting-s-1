###Service
Service FinOS::ESG::services::env::climate::getPortfolioEmissions
{
  pattern: '/finos/esg/env/climate/getPortfolioEmissions';
  owners:
  [
    'dustanm',
    'nanda-GS'
  ];
  documentation: 'Example service to get aggregated emissions at the portfolio level.';
  autoActivateUpdates: true;
  execution: Single
  {
    query: |FinOS::ESG::domain::Pos::Portfolio.all()->groupBy(
      [
        x|$x.portfolioId,
        x|$x.portfolioDate,
        x|$x.assetEntity.esgMetricsEntity.fiscalYear
      ],
      [
        agg(
          x|($x.assetEntity.esgMetricsEntity.env.climate.emissions.ghgScope1Absolute.value * $x.notionalUsd) /
            $x.assetEntity.esgMetricsEntity.fin.evicUsd,
          x|$x->sum()
        )
      ],
      [
        'portfolioId',
        'portfolioDate',
        'fiscalYear',
        'portfolioGhgScope1Emissions'
      ]
    );
    mapping: FinOS::ESG::mapping::CuratedESGMapping;
    runtime:
    #{
      mappings:
      [
        FinOS::ESG::mapping::CuratedESGMapping,
        FinOS::ESG::mapping::ESGBookRelationalMapping,
        ESGBook::mapping::ESGBookRawMapping
      ];
      connections:
      [
        FinOS::ESG::store::CombinedESGStore:
        [
          connection_1: FinOS::ESG::connection::H2ESGConnection
        ]
      ];
    }#;
  }
}
