###Service
Service FinOS::ESG::services::env::climate::getPortfolioIntensity
{
  pattern: '/finos/esg/env/climate/getPortfolioIntensity';
  owners:
  [
    'dustanm',
    'nanda-GS'
  ];
  documentation: 'Example service to get aggregated Intensity at the portfolio level.';
  autoActivateUpdates: true;
  execution: Single
  {
    query: |FinOS::ESG::domain::Pos::Portfolio.all()->project(
      [
        x|$x.portfolioId,
        x|$x.portfolioDate,
        x|$x.assetEntity.esgMetricsEntity.fiscalYear,
        x|$x.notionalUsd,
        x|$x.assetEntity.esgMetricsEntity.fin.evicUsd,
        x|$x.assetEntity.esgMetricsEntity.env.climate.emissions.ghgScope1Absolute.value
      ],
      [
        'portfolioId',
        'portfolioDate',
        'fiscalYear',
        'notionalUsd',
        'evicUsd',
        'ghgScope1Absolute'
      ]
    )->groupBy(
      [
        'portfolioId',
        'portfolioDate',
        'fiscalYear'
      ],
      [
        'portfolioGhgScope1IntensitySum'->agg(
        x|($x.getFloat('notionalUsd') * $x.getFloat('ghgScope1Absolute')) /
          $x.getFloat('evicUsd'),
        y|$y->sum()
      ),
        'portfolioNotionalSum'->agg(
        x|$x.getFloat('notionalUsd'),
        y|$y->sum()
      )
      ]
    )->extend(
      [
        col(
          r: TDSRow[1]|$r.getFloat('portfolioGhgScope1IntensitySum') /
            $r.getFloat('portfolioNotionalSum'),
          'portfolioGhgScope1Intensity'
        )
      ]
    )->restrict(
      [
        'portfolioId',
        'portfolioDate',
        'fiscalYear',
        'portfolioGhgScope1Intensity'
      ]
    );
    mapping: FinOS::ESG::mapping::CuratedESGMapping;
    runtime:
    #{
      mappings:
      [
        FinOS::ESG::mapping::CuratedESGMapping,
        FinOS::ESG::mapping::ESGBookRelationalMapping,
        ESGBook::mapping::ESGBookRawMapping
      ];
      connections:
      [
        FinOS::ESG::store::CombinedESGStore:
        [
          connection_1: FinOS::ESG::connection::H2ESGConnection
        ]
      ];
    }#;
  }
}
