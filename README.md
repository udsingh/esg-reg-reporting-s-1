# ESG data industrialization hackathon

> :warning: **Warning** This initiative is in a *formation* stage; as such:
> - The codebase is not intended (nor ready) for production purposes
> - Not all code hosted in this codebase is covered by [FINOS Contribution License Agreement](https://community.finos.org/docs/governance/Software-Projects/contribution-compliance-requirements)
> - Code is not subject to standard [FINOS compliance and security scans](https://github.com/finos/code-scanning)

## Description
**ESG data industrialization** is run by the RegTech Council which supports the [FINOS Open RegTech Strategic Initiative](https://www.finos.org/open-regtech), and it has been identified as a top priority for 2024 as it will reduce cost and increase compliance for a ‘greenfield’ regulatory reporting mandate.

This codebase will be used to host a hackathon on 8 November 2023, involving all participants listed below; logistics info follows.

### Hackathon Logistics
> :warning: The Hackathon will run under Chatham House [Rule](https://www.chathamhouse.org/about-us/chatham-house-rule#:~:text=The%20Rule%20reads%20as%20follows,other%20participant%2C%20may%20be%20revealed.) to allow participants to collaborate in a spirit of openness.
> Please review the [contributing section below](#contributing) in order to understand how to contribute during the Hackathon.

- Up to 40 physical seats are available at a venue in central London from 8:30am – 17:30 pm (GMT) on Wednesday 8 November
- Virtual participation is possible during those hours (via Zoom)
- Teams will be agreed and assigned by Friday, 3 November 
- Read-ahead materials will be distributed prior to the event
- A detailed agenda for the day will be made available to registrants
- Public WiFi will be available on 8 November and some screens will be avaialbe upon advance requests
- Working in the open is not a strict requirement to participate and it may be possible to work privately and demonatrate results directly from a workstation 
- Registration is managed by PJ Di Giammarino, FINOS RegTech Council Programme Manager (email [pj@jwg-it.eu](mailto:pj@jwg-it.eu))

## Challenge
Enable banks to consume 3rd party ESG data for the purposes of EU regulatory reporting more efficiently through a shared data model and translation tools.
- Banks agree an opensource schema to map data inputs to regulatory outputs
- Tooling to reduce mapping risk between reporting schema
- Data lineage between producers and consumers which meets quality measures

## Benefits
Reduce cost through mutualized efforts, reduce operational risk of compliance and achieve commercially viable results in a better, faster, safer and lower-cost manner. Those at the table will:
- Opensource Data model/ definitions for NFRD, CSRD and SFDR reports
- Shape the technical approach and methodology
- Refine objectives and plans for 2024

## Support
For questions, please email help@finos.org

## Participants
- Allianz Investment Management
- Aviru
- Bloomberg
- Bank of Montreal
- BR-AG
- CDP
- Climate Arc
- ESG Book
- EY
- FINOS
- Goldman Sachs
- JWG Group
- NTT data 
- PwC
- Royal Bank of Canada
- S&P 
- Solidatus
- Watershed 

## Tooling overview

- Legend: 'Legend 101' (45 mins) and 'Legend Deep Dive' (29 mins) walkthroughs [here](https://www.finos.org/legend) with more information in the following sections 
- Solidatus website [here](https://www.solidatus.com/) and ESG specific content [here](https://www.solidatus.com/solutions/esg/) and [here](https://assets-us-01.kc-usercontent.com/0c347726-a638-0036-3d8e-afe5618e1f63/37de92b0-d0d0-4467-9287-5dce666b4232/Streamline%20ESG%20compliance%20-%20Factsheet.pdf) 
- ATOME [here](https://br-ag.eu/products/atome-matter/) 
- Aviru [here](https://vimeo.com/881605990) - instructions [here](https://arivu.io/wp-content/uploads/2023/11/EasyWriter-API-Instructions.pdf) 
- RegDelta: Overview [here] (https://regdelta.jwg-it.eu/), ESG video (2 min) [here](https://regdelta.jwg-it.eu/esg/), flier [here](https://jwg-it.eu/wp-content/uploads/2023/11/RegDelta_ESGv4.pdf), user guide [here](https://jwg-it.eu/wp-content/uploads/2023/11/2023-RegDelta-User-Guide-ESG-Hackathon.pdf)

## Contributions 
> **Note** Ahead of the Hackathon event, please make sure to validate with your manager what is your
> employer's position with open source participation, in order to understand which type of collaboration can
> be set up.

Code and models are publicly hosted on https://gitlab.com/-/ide/project/finosfoundation/legend/reg-innovation/esg-reg-reporting; if you're interested to contributing to this initiative, please read our [Contributing](CONTRIBUTING.md) and [Code of Conduct](CODE_OF_CONDUCT.md) guidelines carefully.

The FINOS Legend Shared instance - available on https://legend.finos.org/studio - provides an IDE to work on the project; to get access to the environment, you'll need to fill in and submit the form on [https://www.finos.org/legend](https://www.finos.org/legend).

Please keep in mind that:
- If you are NOT covered by a FINOS CLA (either Individual or Corporate), you'll be granted with read-only access (`Reporter` role)
- If you are (covered by a FINOS CLA), you'll be allowed to **submit** change (or Merge) requests (`Developer` role)
- If you are one of the FINOS initiative's maintainer, you'll be allowed to **accept** change requests (`Maintainer` role)

To sign the FINOS CLA, you could simply raise a Pull Request against https://github.com/finos/open-developer-platform/blob/main/EASYCLA_CHANGEME.md and follow instructions; for more info, please visit [our community website](https://community.finos.org/docs/governance/software-projects/contribution-compliance-requirements/#contributor-license-agreement).

### Using a private Legend project
If - for any reason - you're not ready/comfortable to work in the open, you can create a private Legend workspace, by following these simple steps:
- [Sign up to Gitlab](https://gitlab.com/users/sign_in); if you don't have an account, you'll need to [register](https://gitlab.com/users/sign_up) first
- [Fork this project](https://gitlab.com/finosfoundation/legend/reg-innovation/esg-reg-reporting/-/forks/new)
- Set your forked project to private: in the left column menu, click on `Settings > General`, then `Expand` the section `Visibility, project features, permissions` and set `Project visibility` to `Private`
- Mark your fork as a Legend project: in the left column menu, click on `Settings > General`, then - under the first section, called `Naming, topics, avatar`, add `legend` in the `topics` field, and `Save changes`
- Optionally, in the same configuration section, you can change the name of the project, by appending a `(<user name> private fork)`

At this point, when visiting https://legend.finos.org/studio and opening the `Projects` dropdown, you should be able to see and select `ESG Reg Reporting <user name> private fork`

Once in the project there is a ReadMeNotes file with more information https://legend.finos.org/studio/view/UAT-51104062/entity/Hackathon::ReadMeNotes

## License
Copyright 2023 Fintech Open Source Foundation

Distributed under the Apache License, Version 2.0.

SPDX-License-Identifier: Apache-2.0

## Roadmap
- [ ] 19 October Hackathon pre-meeting ([ESG1](https://github.com/finos/open-regtech-sig/discussions/61))
- [ ] 31 October - registrations due 
- [ ] 31 October - use case pack [meeting](https://jwg-it.eu/events/finos-regtech-council-esg-data-industrialisation-use-cases-2/) 
- [ ] 2 November - Teams and tooling [meeting](https://jwg-it.eu/events/finos-regtech-council-esg-data-industrialisation-teams-and-tooling/) 
- [ ] 3 November - participant preparation email
- [ ] 8 November - day of the hackathon in [London](https://jwg-it.eu/events/finos-regtech-council-esg-hackathon-2/) 
- [ ] Mid November - wrap up, accomplishments, next steps
- [ ] December - announce ESG 2024 plans to FINOS community 
- [ ] Q1 2024 - share code publicly, under Community Specification License, accessible via FINOS Legend Shared instance
